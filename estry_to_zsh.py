import sys
import os
from pathlib import Path
from qgis.core import QgsApplication, QgsVectorLayer, QgsProject


__version__ = 0.1

# inputs
SCRIPT_LOCATION = r"E:\python\estry_to_xyz"
OUTPUT_LOCATION = r"E:\python\estry_to_xyz\output"

DRIVER = "ESRI Shapefile"


def estry_to_zsh(running_in_qgis, input_=None):
    """ Converts ESTRY cross-section database into 2d_zsh points and lines"""

    sys.path.append(SCRIPT_LOCATION)
    import estrydb_data_provider
    import helpers

    if input_ is None:
        input_ = Path(iface.activeLayer().dataProvider().dataSourceUri())
    outputPoints = Path(OUTPUT_LOCATION) / '2d_zsh_estry_to_zsh_output_P.shp'
    outputLines = Path(OUTPUT_LOCATION) / '2d_zsh_estry_to_zsh_output_L.shp'

    lyr = QgsVectorLayer(str(input_), input_.name, "ogr")
    if not helpers.is1dTable(lyr):
        print("ERROR: Input layer {0} is not a valid 1d_xs layer".format(lyr.name()))
        sys.exit(0)

    xs_layer = estrydb_data_provider.XS_layer(lyr)
    outPointLyr = helpers.create_empty_2d_zsh(outputPoints, lyr.crs(), "P")
    outLineLyr = helpers.create_empty_2d_zsh(outputLines, lyr.crs(), "L")
    if outPointLyr is None or outLineLyr is None:
        print("ERROR: Issue initialising empty output layers")
        sys.exit(0)
    for xs in xs_layer.xs:
        points = estrydb_data_provider.XS.xs_to_points(xs, lyr.crs())
        helpers.addPointsToZsh(points, outPointLyr, outLineLyr, xs.source)

    helpers.writeLayer(outPointLyr, str(outputPoints), DRIVER)
    if not os.path.exists(outputPoints):
        print("ERROR: Issue writing {0}".format(outputPoints))

    helpers.writeLayer(outLineLyr, str(outputLines), DRIVER)
    if not os.path.exists(outputLines):
        print("ERROR: Issue writing {0}".format(outputLines))

    # bring into QGIS
    if running_in_qgis:
        if os.path.exists(outputPoints):
            del outPointLyr
            outPointLyr = QgsVectorLayer(str(outputPoints), outputPoints.name, "ogr")
        if os.path.exists(outputLines):
            del outLineLyr
        outLineLyr = QgsVectorLayer(str(outputLines), outputLines.name, "ogr")
        QgsProject.instance().addMapLayer(outLineLyr)
        QgsProject.instance().addMapLayer(outPointLyr)

    print("Finished")

if __name__ == '__console__':
    estry_to_zsh(True)
elif __name__ == '__main__':
    # for debugging in an IDE
    argv = [bytes(x, 'utf-8') for x in sys.argv]
    qgis = QgsApplication(argv, False)
    qgis.initQgis()
    estry_to_zsh(False, Path(r"E:\_Example_Models_QGIS\TUFLOW\model\xs\1d_xs_EG07_creek_002_L.shp"))
